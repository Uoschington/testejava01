package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Aluno;

public class AlunoDAO{
    private DataSource dataSource;
    
    public AlunoDAO(DataSource dataSource){
        this.dataSource = dataSource;
    }
    
    public ArrayList<Aluno>readAll(){
        try{
            String SQL = "SELECT * from clientes";
            PreparedStatement ps = dataSource.getConnection().prepareStatement(SQL);
            ResultSet rs = ps.executeQuery();

            ArrayList<Aluno> lista = new ArrayList<Aluno>();
            while(rs.next()){
                Aluno cli = new Aluno();
                cli.setId(rs.getInt("ra"));
                cli.setNome(rs.getString("nome"));
                cli.setEmail(rs.getString("email"));
                cli.setTelefone(rs.getString("telefone"));
                lista.add(cli);
            }
            ps.close();
            return lista;
            
        }
        catch (SQLException ex){
            System.err.println("Erro ao recuperar "+ex.getMessage());
        }
        catch (Exception ex){
            System.err.println("Erro geral "+ex.getMessage());
        }
        return null;
    }
    
}